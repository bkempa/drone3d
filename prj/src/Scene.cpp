#include "Scene.hh"

Scene::Scene(drawNS::APIopenGL3D *_api) {
    api = _api;
    Hill hill_start;
    Plateau plat_start;
    Plateau_Rect rec_plat_start;
    Drone dr_start;
    std::shared_ptr<Hill> temp_hill (hill_start.add_shape(Vector<3>({10,12,7.5}),15,4,10,api));
    std::shared_ptr<Drawing_Interface> temp_hill2 = std::dynamic_pointer_cast<Drawing_Interface>(temp_hill);
    std::shared_ptr<Plateau> temp_pl (plat_start.add_shape(Vector<3>({-12,-4,3.5}),7,8,12,api));
    std::shared_ptr<Drawing_Interface> temp_pl2 = std::dynamic_pointer_cast<Drawing_Interface>(temp_pl);
    std::shared_ptr<Plateau_Rect> temp_rec_pl (rec_plat_start.add_shape(Vector<3>({10,-10,3}),Rotation_Matrix<3>(38),6,16,10,api));
    std::shared_ptr<Drawing_Interface> temp_rec_pl2 = std::dynamic_pointer_cast<Drawing_Interface>(temp_rec_pl);
    std::shared_ptr<Drone> temp_dr (dr_start.add_shape(Vector<3>({-10,15,1}),Rotation_Matrix<3>(50),api));
    std::shared_ptr<Drawing_Interface> temp_dr2 = std::dynamic_pointer_cast<Drawing_Interface>(temp_dr);
    drawable_elements.push_back(temp_hill2);
    scenery_elements.push_back(std::dynamic_pointer_cast<Scenery_Element_Interface>(temp_hill2));
    drawable_elements.push_back(temp_pl2);
    scenery_elements.push_back(std::dynamic_pointer_cast<Scenery_Element_Interface>(temp_pl2));
    drawable_elements.push_back(temp_rec_pl2);
    scenery_elements.push_back(std::dynamic_pointer_cast<Scenery_Element_Interface>(temp_rec_pl2));
    drawable_elements.push_back(temp_dr2);
    drones.push_back(std::dynamic_pointer_cast<Drone_Interface>(temp_dr2));
    current = temp_dr;
    draw_everything();
}


void Scene::animate(const double & altitude, const double & angle, const double & distance){
    if (current != nullptr){
        const double temporary = 0.1;
        const int delay = 16;
        double alt = 0, dist_temp = 0;
        while (alt < fabs(altitude)){
            alt += temporary;
            if(altitude > 0){
                current->fly_vertically(temporary,api);
                current->move_rotors();
                std::this_thread::sleep_for(std::chrono::milliseconds(delay));
            } else if (altitude < 0) {
                current->fly_vertically(-temporary,api);
                current->move_rotors();
                std::this_thread::sleep_for(std::chrono::milliseconds(delay));
            }
        }
        //rotate
        const double temporary_rot = 1;
        for (int i = 0; i <= int(fabs(angle)/temporary_rot); ++i){
            if (angle > 0) {
                current->rotate(temporary_rot,api);
                current->move_rotors();
                std::this_thread::sleep_for(std::chrono::milliseconds(delay));
            } else if (angle < 0) {
                current->rotate(-temporary_rot,api);
                current->move_rotors();
                std::this_thread::sleep_for(std::chrono::milliseconds(delay));
            }
        }
        while (dist_temp < fabs(distance)){
            dist_temp += temporary;
            if(distance > 0){
                current->fly_forward(temporary,api);
                current->move_rotors();
                std::this_thread::sleep_for(std::chrono::milliseconds(delay));
            } else if (distance < 0) {
                current->fly_forward(-temporary,api);
                current->move_rotors();
                std::this_thread::sleep_for(std::chrono::milliseconds(delay));
            }
        }
        double temp = 0;
        if (check_collision(temp)) {
            alt = 0;
            if (!((angle == 0 && distance == 0 && altitude > 0) || (distance == 0 && altitude == 0 && angle != 0) || (distance != 0 && altitude == 0 && angle == 0))){
                    while (alt < fabs(temp)){
                        alt += temporary;
                        if(altitude > 0){
                            current->fly_vertically(-temporary,api);
                            current->move_rotors();
                            std::this_thread::sleep_for(std::chrono::milliseconds(delay));
                        } 
                    }
            }
        }
        else
            std::cout << "W tym miejscu nie mozna wyladowac" << std::endl;
    }
    else
        std::cout << "Nie ma zadnych dronow" << std::endl;
}

void Scene::draw_everything() {
    for (const auto & x : drawable_elements)
        x->draw();
}

bool Scene::check_collision(double &distance) const {
    std::vector<int> obstructions_id;
    for (ulong i=0; i<scenery_elements.size(); i++) { //wybieranie najblizszych przeszkodod
        if (std::dynamic_pointer_cast<Scenery_Element_Interface>(current) != scenery_elements[i]) {
            Vector<3> temp1 = std::dynamic_pointer_cast<Drawing_Interface>(scenery_elements[i])->centre_position();
            Vector<3> temp2 = std::dynamic_pointer_cast<Drawing_Interface>(current)->centre_position();
            double dist = sqrt(pow(temp1[0] - temp2[0],2) + pow(temp1[1] - temp2[1],2));
            if (dist < scenery_elements[i]->get_R() + std::dynamic_pointer_cast<Scenery_Element_Interface>(current)->get_R() * 1.2)
                obstructions_id.push_back(i);
        }
    }
    if (!obstructions_id.empty()) {
        for (int i : obstructions_id) {
            if (!scenery_elements[i]->if_above(current))
                obstructions_id[i] = -1;
        }
        for (int i : obstructions_id) {
            if (i != -1) {
                if (!scenery_elements[i]->if_land(current, distance))
                    return false;
            }
        }
        return true;
    } else {
        distance = std::dynamic_pointer_cast<Drawing_Interface>(current)->centre_position()[2] - 1;
        return true;
    }

}

void Scene::add_surface_element(const char &obstruction){
    Vector<3> obstruction_centre;
    double obstruction_height, obstruction_width, obstruction_depth, obstruction_orientation, Rmin, Rmax;
    switch(obstruction){
        case '1': {
            std::cout << "Enter hill centre's coordinates (the third coordinate is insignificant, the hill will be created on the surface)" << std::endl;
            std::cin >> obstruction_centre;
            std::cout << "Enter height of the hill" << std::endl;
            std::cin >> obstruction_height;
            obstruction_centre[2] = obstruction_height/2;
            std::cout << "Enter minimum oraz maximum distance from a vertice to the centre of the base" << std::endl;
            std::cin >> Rmin;
            std::cin >> Rmax;
            Hill temporary;
            std::shared_ptr<Hill> temp1 (temporary.add_shape(obstruction_centre,obstruction_height,Rmin,Rmax,api));
            std::shared_ptr<Drawing_Interface> temp2 = std::dynamic_pointer_cast<Drawing_Interface>(temp1);
            drawable_elements.push_back(temp2);
            scenery_elements.push_back(std::dynamic_pointer_cast<Scenery_Element_Interface>(temp2));
            break;
        }
        case '2': {
            std::cout << "Enter plateau centre's coordinates (the third coordinate is insignificant, the plateau will be created on the surface)" << std::endl;
            std::cin >> obstruction_centre;
            std::cout << "Enter height of the plateau" << std::endl;
            std::cin >> obstruction_height;
            obstruction_centre[2] = obstruction_height/2;
            std::cout << "Enter minimum oraz maximum distance from a vertice to the centre of the base" << std::endl;
            std::cin >> Rmin;
            std::cin >> Rmax;
            Plateau temporary;
            std::shared_ptr<Plateau> temp1 (temporary.add_shape(obstruction_centre,obstruction_height,Rmin,Rmax,api));
            std::shared_ptr<Drawing_Interface> temp2 = std::dynamic_pointer_cast<Drawing_Interface>(temp1);
            drawable_elements.push_back(temp2);
            scenery_elements.push_back(std::dynamic_pointer_cast<Scenery_Element_Interface>(temp2));
            break;
        }
        case '3': {
            std::cout << "Enter rect. plateau centre's coordinates (the third coordinate is insignificant, the plateau will be created on the surface)" << std::endl;
            std::cin >> obstruction_centre;
            obstruction_centre[2] = 0;
            std::cout << "Enter height of the rect. plateau" << std::endl;
            std::cin >> obstruction_height;
            obstruction_centre[2] = obstruction_height/2;
            std::cout << "Enter width of the plateau" << std::endl;
            std::cin >> obstruction_width;
            std::cout << "Enter depth of the plateau" << std::endl;
            std::cin >> obstruction_depth;
            std::cout << "Enter rotation angle (Z-axis) of the plateau" << std::endl;
            std::cin >> obstruction_orientation;
            Rotation_Matrix<3> mat(obstruction_orientation);
            Plateau_Rect temporary;
            std::shared_ptr<Plateau_Rect> temp1 (temporary.add_shape(obstruction_centre,mat,obstruction_height,obstruction_width,obstruction_depth,api));
            std::shared_ptr<Drawing_Interface> temp2 = std::dynamic_pointer_cast<Drawing_Interface>(temp1);
            drawable_elements.push_back(temp2);
            scenery_elements.push_back(std::dynamic_pointer_cast<Scenery_Element_Interface>(temp2));
            break;
        }
        default:
            std::cout << "Available options: 1 - hill, 2 - plateau, 3 - rectangular plateau" << std::endl;
    }
}

void Scene::remove_surface_element(const uint &id) {
    uint temp = id-1;
    std::shared_ptr<Scenery_Element_Interface> el_ptr = scenery_elements[temp];
    std::shared_ptr<Drawing_Interface> draw_ptr = std::dynamic_pointer_cast<Drawing_Interface>(el_ptr);
    draw_ptr->remove();
    drawable_elements.erase(std::remove(drawable_elements.begin(), drawable_elements.end(), draw_ptr), drawable_elements.end());
    scenery_elements.erase(scenery_elements.begin() + temp);
    draw_everything();
}

void Scene::display_scenery_elements() const {
    for(uint i = 0; i < scenery_elements.size(); ++i)
       std::cout << "Element with index: " << i+1 << ". Centre coordinates:" << std::endl << std::dynamic_pointer_cast<Drawing_Interface>(scenery_elements[i])->centre_position();
}

void Scene::add_drone(Vector<3> &drone_centre, const Rotation_Matrix<3> &rot_mat, drawNS::APIopenGL3D *api){
    Drone temporary;
    if (drone_centre[2] == 0)
        drone_centre[2] = 1;
    std::shared_ptr<Drone> temp1 (temporary.add_shape(drone_centre,rot_mat,api));
    std::shared_ptr<Drawing_Interface> temp2 = std::dynamic_pointer_cast<Drawing_Interface>(temp1);
    drawable_elements.push_back(temp2);
    drones.push_back(std::dynamic_pointer_cast<Drone_Interface>(temp2));
    current = temp1;
    for (const auto &i : drawable_elements)
        i->draw();
}

void Scene::remove_drone(const uint &id){
    uint temp = id-1;
    std::shared_ptr<Drone_Interface> dr_ptr = drones[temp];
    std::shared_ptr<Drawing_Interface> draw_ptr = std::dynamic_pointer_cast<Drawing_Interface>(dr_ptr);
    draw_ptr->remove();
    drawable_elements.erase(std::remove(drawable_elements.begin(), drawable_elements.end(), draw_ptr), drawable_elements.end());
    drones.erase(drones.begin() + temp);
    draw_everything();
    if (temp == 0)
        current = drones[0];
}

void Scene::display_drones() const {
    for (uint i = 0; i < drones.size(); ++i)
        std::cout << "Drone with index: " << i+1 << ". Centre coordinates:" << std::endl << std::dynamic_pointer_cast<Drawing_Interface>(drones[i])->centre_position();
}

void Scene::choose_drone(const uint &id){
    uint temp = id - 1;
    current = drones[temp];
}
