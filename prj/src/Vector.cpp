#include "Vector.hh"

template <int SIZE>
Vector<SIZE> Vector<SIZE>::operator+(const Vector & arg2) const {
    Vector<SIZE> result;
    for (int i=0; i < SIZE; ++i)
        result[i] = arr[i] + arg2[i];
    return result;
}


template <int SIZE>
Vector<SIZE> Vector<SIZE>::operator-(const Vector & arg2) const {
    Vector<SIZE> result;
    for (int i=0; i < SIZE; ++i)
        result[i] = arr[i] - arg2[i];
    return result;
}


template <int SIZE>
Vector<SIZE> Vector<SIZE>::operator*(double arg2) const {
    Vector<SIZE> result;
    for (int i = 0; i < SIZE; ++i)
        result[i] = arr[i] * arg2;
    return result;
}


template <int SIZE>
double Vector<SIZE>::operator*(const Vector<SIZE> &arg2) const {
    double result;
    for (int i = 0; i < SIZE; ++i)
        result += arr[i] * arg2[i];
    return result;
}


template <int SIZE>
const double & Vector<SIZE>::operator[](int ind) const {
    if (ind < 0 || ind > (SIZE-1)) {
        std::cerr << "Index out of bounds" << std::endl;
        exit(0);
    }
    else
        return arr[ind];
}


template <int SIZE>
double & Vector<SIZE>::operator[](int ind) {
    if (ind < 0 || ind > (SIZE-1)) {
        std::cerr << "Index out of bounds" << std::endl;
        exit(0);
    }
    else
        return arr[ind];
}


template <int SIZE>
double Vector<SIZE>::length() const {
    double el;
    for (int i = 0; i < SIZE; ++i)
        el += pow(arr[i], 2);
    return sqrt(el);
}


template <int SIZE>
std::istream& operator >> (std::istream &Strm, Vector<SIZE> &vec) {
    for (int i = 0; i < SIZE; ++i)
        Strm >> vec[i];
    return Strm;
}

template <int SIZE>
std::ostream& operator << (std::ostream &Strm, const Vector<SIZE> &vec) {
    for (int i = 0; i < SIZE; ++i)
        Strm << "[ " << vec[i] << " ]" << std::endl;
    return Strm;
}


template class Vector<3>;
template class Vector<2>;
template std::ostream& operator << (std::ostream &Strm, const Vector<3> &vec);
template std::istream& operator >> (std::istream &Strm, Vector<3> &vec);