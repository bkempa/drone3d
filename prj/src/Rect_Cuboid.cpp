#include "Rect_Cuboid.hh"


void Rect_Cuboid::draw(){
    if (id == -1) {
        double hgh = height / 2;
        double wdt = width / 2;
        double dpt = depth / 2;
        Vector<3> WD1({(-1) * wdt, (-1) * dpt, (-1) * hgh});    //LD
        Vector<3> WD2({wdt, (-1) * dpt, (-1) * hgh});   //PD
        Vector<3> WD3({wdt, dpt, (-1) * hgh});  //PG
        Vector<3> WD4({(-1) * wdt, dpt, (-1) * hgh});   //LG
        Vector<3> WG1({(-1) * wdt, (-1) * dpt, hgh});
        Vector<3> WG2({wdt, (-1) * dpt, hgh});
        Vector<3> WG3({wdt, dpt, hgh});
        Vector<3> WG4({(-1) * wdt, dpt, hgh});
        std::vector <drawNS::Point3D> top;
        std::vector <drawNS::Point3D> bottom;
        top = {convert(point_in_predecessor(WD1)), convert(point_in_predecessor(WD2)),
               convert(point_in_predecessor(WD3)), convert(point_in_predecessor(WD4)),
               convert(point_in_predecessor(WD1))};
        bottom = {convert(point_in_predecessor(WG1)), convert(point_in_predecessor(WG2)),
                convert(point_in_predecessor(WG3)), convert(point_in_predecessor(WG4)),
                convert(point_in_predecessor(WG1))};
        std::vector <std::vector<drawNS::Point3D>> final;
        final.push_back(top);
        final.push_back(bottom);
        for (auto i : final[1])
            vertices.push_back(i);
        id = api->draw_polyhedron(final, "orange");
    }
}


void Rect_Cuboid::remove() {
    api->erase_shape(id);
}



void Rect_Cuboid_Drone::draw(){
    double hgh = height/2;
    double wdt = width/2;
    double dpt = depth/2;
    Vector<3> WD1({(-1) * wdt, (-1) * dpt, (-1) * hgh});
    Vector<3> WD2({wdt, (-1) * dpt, (-1) * hgh});
    Vector<3> WD3({wdt, dpt, (-1) * hgh});
    Vector<3> WD4({(-1) * wdt, dpt, (-1) * hgh});
    Vector<3> WG1({(-1) * wdt, (-1) * dpt, hgh});
    Vector<3> WG2({wdt, (-1) * dpt, hgh});
    Vector<3> WG3({wdt, dpt, hgh});
    Vector<3> WG4({(-1) * wdt, dpt, hgh});
    std::vector<drawNS::Point3D> top;
    std::vector<drawNS::Point3D> bottom;
    top = {convert(point_in_predecessor(WD1)), convert(point_in_predecessor(WD2)), convert(point_in_predecessor(WD3)), convert(point_in_predecessor(WD4)), convert(point_in_predecessor(WD1))};
    bottom = {convert(point_in_predecessor(WG1)), convert(point_in_predecessor(WG2)), convert(point_in_predecessor(WG3)), convert(point_in_predecessor(WG4)), convert(point_in_predecessor(WG1))};
    std::vector<std::vector<drawNS::Point3D> > final;
    final.push_back(top);
    final.push_back(bottom);

    if (id == -1)
        id = api->draw_polyhedron(final, "black");
    else {
        id2 = id;
        id = api->draw_polyhedron(final, "black");
    }
}


void Rect_Cuboid_Drone::remove() {
    if (id2 != -1) {
        api->erase_shape(id2);
        id2 = -1;
    }
    else {
        api->erase_shape(id);
        id = -1;
    }
}
