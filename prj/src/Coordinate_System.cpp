#include "Coordinate_System.hh"

Vector<3> Coordinate_System::point_in_predecessor(const Vector<3> &point) const {
    Vector<3> result;
    if (predecessor != nullptr)
        result = predecessor->centre + predecessor->orientation * (centre + orientation*point);
    else
        result = centre + orientation * point;
    return result;
}



Coordinate_System Coordinate_System::system_in_global() const {
    if (predecessor != nullptr) {
        Coordinate_System temp = predecessor->system_in_global();
        temp.translation(centre);
        temp.rotation(orientation);
        return temp;
    }
    else 
        return *this;
}