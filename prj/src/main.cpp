#include <iostream>
#include "Drone.hh"
#include "Surface.hh"
#include "Scene.hh"


void display_menu() {
    std::cout << "\tDrone:" << std::endl;
    std::cout << "s - sequence of moves animation" << std::endl;
    std::cout << "v - fly vertically" << std::endl;
    std::cout << "f - fly forward on given distance" << std::endl;
    std::cout << "r - drone rotation around Z-axis" << std::endl;
    std::cout << "i - display indexes and drones' centres positions" << std::endl;
    std::cout << "c - choose drone" << std::endl;
    std::cout << "a - add drone" << std::endl;
    std::cout << "e - remove drone" << std::endl << std::endl;
    std::cout << "\tScenery elements:" << std::endl;
    std::cout << "y - display indexes and scenery elements' centres positions" << std::endl;
    std::cout << "l - add scenery element" << std::endl;
    std::cout << "t - remove scenery element" << std::endl << std::endl;
    std::cout << "\tOther:" << std::endl;
    std::cout << "n - display number of vectors" << std::endl;
    std::cout << "m - display menu" << std::endl;
    std::cout << "q - quit" << std::endl << std::endl;
}

int main(int argc, char **argv){
    drawNS::APIopenGL3D * api = new drawNS::APIopenGL3D(-25,25,-25,25,-5,45,0,&argc,argv);
    Surface P(0);
    P.draw(api);
    Vector<3> drone_centre;
    Scene S(api);
    char menu, obstruction;
    double distance, height, angle;
    std::cout << "\t\t\t\t\tMENU" << std::endl << std::endl;
    display_menu();
    std::cout << "Attention - commands to facilitate tests:" << std::endl;
    std::cout << "\tTo land on the rectangular plateau: s -> 10 -> -100 -> 30." << std::endl;
    std::cout << "Then:" << std::endl;
    std::cout << "\tTo land on the plateau: s -> 5 -> -140 -> 20." << std::endl;
    std::cout << "Then:" << std::endl;
    std::cout << "\tTo land on the hill: s -> 15 -> -130 -> 25." << std::endl << std::endl;
    std::cout << "Your choice: " << std::endl;
    std::cin >> menu;
    while (menu != 'q'){
        switch(menu){
            case 's':
                std::cout << "\tEnter drone moves sequence" << std::endl;
                std::cout << "Enter value to change drone's height: ";
                std::cin >> height;
                std::cout << "Enter rotation angle: ";
                std::cin >> angle;
                std::cout << "Enter flight distance: ";
                std::cin >> distance;
                S.animate(height,angle,distance);
                break;
            case 'v':
                std::cout << "Enter value to change drone's height: ";
                std::cin >> height;
                S.animate(height,0,0);
                break;
            case 'f':
                std::cout << "Enter flight distance: ";
                std::cin >> distance;
                S.animate(0,0,distance);
                break;
            case 'r':
                std::cout << "Enter rotation angle: ";
                std::cin >> angle;
                S.animate(0,angle,0);
                break;
            case 'm':
                display_menu();
                break;
            case 'n':
                Vector<3>::display();
                break;
            case 'c': {
                std::cout << "Enter ID of the drone that you want to control: (press 0 to display all available drones)" << std::endl;
                uint id;
                std::cin >> id;
                if(id == 0){
                    S.display_drones();
                    std::cout << "Enter ID of the drone that you want to control:" << std::endl;
                    std::cin >> id;
                }
                S.choose_drone(id);
                break;
            }
            case 'a': {
                std::cout << "Enter new drone's centre coordinates (format: x y z): (if you want to create the drone on the surface -> z = 1, the newly created drone is set as active)" << std::endl;
                std::cin >> drone_centre;
                std::cout << "Enter drone's rotation angle (rotation around Z-axis in degrees):" << std::endl;
                std::cin >> angle;
                Rotation_Matrix<3> M_Obr(angle);
                S.add_drone(drone_centre, M_Obr, api);
                S.draw_everything();
                break;
            }
            case 'e': {
                std::cout << "Enter ID of the drone that you want to remove: (press 0 to display all available drones)" << std::endl;
                uint drone_id;
                std::cin >> drone_id;
                if(drone_id == 0) {
                    S.display_drones();
                    std::cout << "Enter ID of the drone that you want to remove:" << std::endl;
                    std::cin >> drone_id;
                }
                S.remove_drone(drone_id);
                break;
            }
            case 't': {
                std::cout << "Enter ID of the obstruction that you want to remove: (press 0 to display all available obstructions)" << std::endl;
                uint obstruction_id;
                std::cin >> obstruction_id;
                if(obstruction_id == 0) {
                    S.display_scenery_elements();
                    std::cout << "Enter ID of the obstruction that you want to remove:" << std::endl;
                    std::cin >> obstruction_id;
                }
                S.remove_surface_element(obstruction_id);
                break;
            }
            case 'l': 
                std::cout << "Choose obstruction: 1 - hill, 2 - plateau, 3 - rectangular plateau" << std::endl;
                std::cin >> obstruction;
                S.add_surface_element(obstruction);
                S.draw_everything();
                break;
            case 'i':
                S.display_drones();
                break;
            case 'y':
                S.display_scenery_elements();
                break;
            default:
                std::cout << "Choose one of the available options" << std::endl;
        }
        std::cout << "Your choice: " << std::endl;
        std::cin >> menu;
    }
    delete api;
}