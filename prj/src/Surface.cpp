#include "Surface.hh"

void Surface::draw(drawNS::APIopenGL3D *_api){
    Vector<3> P1({-25, -25, altitude});
    Vector<3> P2({25, -25, altitude});
    Vector<3> P3({25, 25, altitude});
    Vector<3> P4({-25, 25, altitude});
    std::vector <std::vector<drawNS::Point3D>> points;
    std::vector <drawNS::Point3D> tmp;
    tmp.push_back(convert(P1));
    tmp.push_back(convert(P2));
    points.push_back(tmp);
    tmp.clear();
    tmp.push_back(convert(P4));
    tmp.push_back(convert(P3));
    points.push_back(tmp);
    _api->draw_polyhedron(points, "orange");
}
