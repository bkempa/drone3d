#include "Hill.hh"

std::shared_ptr<Hill> Hill::add_shape(const Vector<3> &centre, const double &height, const double &r_min, const double &r_max, drawNS::APIopenGL3D * _api) {
    std::shared_ptr<Hill> temp = std::make_shared<Hill>(centre,height,r_min,r_max,_api);
    return temp;
}


void Hill::draw() {
    if (id == -1)
        id = api->draw_polyhedron(vertices, "orange");
}


void Hill::remove() {
    api->erase_shape(id);
}

bool Hill::if_above(const std::shared_ptr<Drone_Interface> &D) {
    Vector<3> drone_centre = std::dynamic_pointer_cast<Drawing_Interface>(D)->centre_position();
    double horizontal_dist = sqrt(pow(centre[0]-drone_centre[0],2)+pow(centre[1]-drone_centre[1],2));
    if(horizontal_dist <= this->get_R()+std::dynamic_pointer_cast<Scenery_Element_Interface>(D)->get_R())
        return true;
    else
        return false;
}
