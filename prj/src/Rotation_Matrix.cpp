#include "Rotation_Matrix.hh"

template <int SIZE>
Rotation_Matrix<SIZE>::Rotation_Matrix(double angle, char axis) {
    if (SIZE != 2 || SIZE != 3)
        std::cerr << "Hyperrotations are not possible in the programme" << std::endl;    
}


template <>
Rotation_Matrix<2>::Rotation_Matrix (double angle, char axis) {
    matrix[0][0] = cos(M_PI * angle/180);
    matrix[0][1] = (-1)*sin(M_PI * angle/180);
    matrix[1][0] = (-1)*matrix[0][1];
    matrix[1][1] = matrix[0][0];
}

template <>
Rotation_Matrix<3>::Rotation_Matrix(double angle, char axis) {
    switch (axis) {
        case 'Z':
            matrix[0][0] = cos(M_PI * angle/180);
            matrix[0][1] = (-1)*sin(M_PI * angle/180);
            matrix[0][2] = 0;
            matrix[1][0] = (-1)*matrix[0][1];
            matrix[1][1] = matrix[0][0];
            matrix[1][2] = 0;
            matrix[2][0] = 0;
            matrix[2][1] = 0;
            matrix[2][2] = 1;
            break;
        case 'Y':
            matrix[0][0] = cos(M_PI * angle/180);
            matrix[0][1] = 0;
            matrix[0][2] = sin(M_PI * angle/180);
            matrix[1][0] = 0;
            matrix[1][1] = 1;
            matrix[1][2] = 0;
            matrix[2][0] = (-1)*matrix[0][2];
            matrix[2][1] = 0;
            matrix[2][2] = matrix[0][0];
            break;
        case 'X':
            matrix[0][0] = 1;
            matrix[0][1] = 0;
            matrix[0][2] = 0;
            matrix[1][0] = 0;
            matrix[1][1] = cos(M_PI * angle/180);
            matrix[1][2] = (-1)*sin(M_PI * angle/180);
            matrix[2][0] = 0;
            matrix[2][1] = (-1)*matrix[1][2];
            matrix[2][2] = matrix[1][1];
            break;
        default:
            std::cout << "Available axis: X, Y, Z" << std::endl;
    }
}

template <int SIZE>
Vector<SIZE> Rotation_Matrix<SIZE>::operator*(const Vector<SIZE> & arg2) const {
    Vector<SIZE> result;
    for (int i = 0;i < SIZE; ++i){
        result[i] = 0;
        for (int j = 0; j < SIZE; ++j)
            result[i] += ( matrix[i][j] * arg2[j] );
    }
    return result;
}

template <int SIZE>
Rotation_Matrix<SIZE> Rotation_Matrix<SIZE>::operator*(const Rotation_Matrix<SIZE> & arg2) const {
    Rotation_Matrix<SIZE> result;
    for (int i = 0; i < SIZE; ++i){
        for (int j = 0; j < SIZE; ++j){
            result.matrix[i][j] = 0;
            for (int k = 0; k < SIZE; ++k)
                result.matrix[i][j] += matrix[i][k] * arg2[k][j];
        }
    }
    return result;
}

template <int SIZE>
const Vector<SIZE> & Rotation_Matrix<SIZE>::operator[] (const int &ind) const {
    if (ind < 0 || ind > (SIZE-1)) {
        std::cerr << "Matrix index out of bounds" << std::endl;
        exit(0);
    }
    else
        return matrix[ind];
}

template <int SIZE>
std::ostream& operator << (std::ostream &Strm, const Rotation_Matrix<SIZE> &Mat) {
    for (int i = 0; i < SIZE; ++i) {
        for (int j = 0; j < SIZE; ++j)
            Strm << "| " << Mat[i][j] << " |";
        Strm << std::endl;
    }
    return Strm;
}

template class Rotation_Matrix<3>;
template class Rotation_Matrix<2>;
template std::ostream& operator << (std::ostream &Strm, const Rotation_Matrix<3> &Mat);