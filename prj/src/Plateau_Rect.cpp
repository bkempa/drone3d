#include "Plateau_Rect.hh"

std::shared_ptr<Plateau_Rect> Plateau_Rect::add_shape(const Vector<3> &ctr, const Rotation_Matrix<3> &orientation, const double &hgh, const double &width, const double &depth, drawNS::APIopenGL3D *_api){
    std::shared_ptr<Plateau_Rect> temp = std::make_shared<Plateau_Rect>(ctr,orientation,hgh,width,depth,_api);
    return temp;
}

bool Plateau_Rect::if_above(const std::shared_ptr<Drone_Interface> &D) {
    std::vector<drawNS::Point3D> vert = *((*this).get_vertices());
    double R_drone = std::dynamic_pointer_cast<Scenery_Element_Interface>(D)->get_R();
    Vector<3> center_drone = std::dynamic_pointer_cast<Drawing_Interface>(D)->centre_position();
    Vector<2> drone_xy ({center_drone[0],center_drone[1]});    //vector with drone's centre x and y coordinates
    Vector<2> vec_horizontal({R_drone, 0});
    Vector<2> vec_vertical({0, R_drone});
    std::array<Vector<2>, 4> vertices_act;
    for (int i = 0; i < 4; i++) { //copying base
        vertices_act[i][0] = vert[i][0];
        vertices_act[i][1] = vert[i][1];
    }
    vertices_act[0] = vertices_act[0] - vec_horizontal - vec_vertical;   //proper copy expansion
    vertices_act[1] = vertices_act[1] + vec_horizontal - vec_vertical;
    vertices_act[2] = vertices_act[2] + vec_horizontal + vec_vertical;
    vertices_act[3] = vertices_act[3] - vec_horizontal + vec_vertical;
    std::array<Vector<2>, 4> vec;

    if (vec_horizontal[0] > 0 && vec_horizontal[1] >= 0) { //adjusting coordinates so that the rectangle is oriented properly
        vec[0] = vertices_act[0]; vec[1] = vertices_act[1]; vec[2] = vertices_act[2]; vec[3] = vertices_act[3];
    } else if (vec_horizontal[0] <= 0 && vec_horizontal[1] > 0) {
        vec[0] = vertices_act[3]; vec[1] = vertices_act[0]; vec[2] = vertices_act[1]; vec[3] = vertices_act[2];
    } else if (vec_horizontal[0] < 0 && vec_horizontal[1] <= 0) {
        vec[0] = vertices_act[2]; vec[1] = vertices_act[3]; vec[2] = vertices_act[0]; vec[3] = vertices_act[1];
    } else if (vec_horizontal[0] >= 0 && vec_horizontal[1] < 0) {
        vec[0] = vertices_act[1]; vec[1] = vertices_act[2]; vec[2] = vertices_act[3]; vec[3] = vertices_act[0];
    }

    //creating two straights covering edges of the expanded base
    double a_horizontal, b_horizontal, c_horizontal;
    double a_vertical, b_vertical, c_vertical;
    a_horizontal = vec[0][1] - vec[1][1];
    b_horizontal = vec[1][0] - vec[0][0];
    if (b_horizontal == 0)
        b_horizontal = 0.000000001;
    c_horizontal = vec[1][1] * vec[0][0] - vec[0][1] * vec[1][0];
    a_vertical = vec[3][1] - vec[0][1];
    b_vertical = vec[0][0] - vec[3][0];
    if (b_vertical == 0)
        b_vertical = 0.0001;
    c_vertical = vec[0][1] * vec[3][0] - vec[3][1] * vec[0][0];
    
    //checking if the drone's centre is in the area determined by the two created straights 
    if (drone_xy[1] > -(a_horizontal * drone_xy[0] + c_horizontal) / b_horizontal && fabs(a_horizontal * drone_xy[0] + b_horizontal * drone_xy[1] + c_horizontal) / sqrt(pow(a_horizontal,2) + pow(b_horizontal,2)) < (vec[3] - vec[0]).length()) {
        if (drone_xy[1] > -(a_vertical * drone_xy[0] + c_vertical) / b_vertical && fabs(a_vertical * drone_xy[0] + b_vertical * drone_xy[1] + c_vertical) / sqrt(pow(a_vertical,2) + pow(b_vertical,2)) < (vec[1] - vec[0]).length())
            return true;
        else
            return false;
    } else 
        return false;
}

bool Plateau_Rect::if_land(const std::shared_ptr<Drone_Interface> &D, double &alt) {
    std::vector<drawNS::Point3D> vert = *((*this).get_vertices());
    double R_drone = std::dynamic_pointer_cast<Scenery_Element_Interface>(D)->get_R();
    Vector<3> center_drone = std::dynamic_pointer_cast<Drawing_Interface>(D)->centre_position();
    Vector<2> C ({center_drone[0], center_drone[1]});
    Vector<2> vec_horizontal ({vert[1][0] - vert[0][0], vert[1][1] - vert[0][1]});
    Vector<2> vec_vertical ({vert[3][0] - vert[0][0], vert[3][1] - vert[0][1]});
    double len_horizontal = vec_horizontal.length();
    double len_vertical = vec_vertical.length();
    vec_horizontal = vec_horizontal * (R_drone / len_horizontal);
    vec_vertical = vec_vertical * (R_drone / len_vertical);
    std::array<Vector<2>, 4> vertices_act;
    for (int i=  0; i < 4; i++) {
        vertices_act[i][0] = vert[i][0];
        vertices_act[i][1] = vert[i][1];
    }
    std::array<Vector<2>, 4> vec;

    if (vec_horizontal[0] > 0 && vec_horizontal[1] >= 0) {
        vec[0] = vertices_act[0]; vec[1] = vertices_act[1]; vec[2] = vertices_act[2]; vec[3] = vertices_act[3];
    } else if (vec_horizontal[0] <= 0 && vec_horizontal[1] > 0) {
        vec[0] = vertices_act[3]; vec[1] = vertices_act[0]; vec[2] = vertices_act[1]; vec[3] = vertices_act[2];
    } else if (vec_horizontal[0] < 0 && vec_horizontal[1] <= 0) {
        vec[0] = vertices_act[2]; vec[1] = vertices_act[3]; vec[2] = vertices_act[0]; vec[3] = vertices_act[1];
    } else if (vec_horizontal[0] >= 0 && vec_horizontal[1] < 0) {
        vec[0] = vertices_act[1]; vec[1] = vertices_act[2]; vec[2] = vertices_act[3]; vec[3] = vertices_act[0];
    }

    double a_horizontal, b_horizontal, c_horizontal, a_vertical, b_vertical, c_vertical;
    a_horizontal = vec[0][1] - vec[1][1];
    b_horizontal = vec[1][0] - vec[0][0];
    if (b_horizontal == 0)
        b_horizontal = 0.000000001;
    c_horizontal = vec[1][1] * vec[0][0] - vec[0][1] * vec[1][0];
    a_vertical = vec[3][1] - vec[0][1];
    b_vertical = vec[0][0] - vec[3][0];
    if (b_vertical == 0)
        b_vertical = 0.0001;
    c_vertical = vec[0][1] * vec[3][0] - vec[3][1] * vec[0][0];

    if (C[1] > -(a_horizontal * C[0] + c_horizontal) / b_horizontal && fabs(a_horizontal * C[0] + b_horizontal * C[1] + c_horizontal) / sqrt(pow(a_horizontal,2) + pow(b_horizontal,2)) < (vec[3] - vec[0]).length()) {
        if (C[1] > -(a_vertical * C[0] + c_vertical)/ b_vertical && fabs(a_vertical * C[0] + b_vertical * C[1] + c_vertical) / sqrt(pow(a_vertical,2) + pow(b_vertical,2)) < (vec[1] - vec[0]).length()) {
            alt = center_drone[2] - vert[0][2] - 1;
            return true;
        } else
            return false;
    } else
        return false;
    return true;
}
