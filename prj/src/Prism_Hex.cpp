#include "Prism_Hex.hh"

Vector<3> Prism_Hex::centre_position() {
    return centre;
}


void Prism_Hex::draw(){
    double r = (radius * sqrt(3))/2;
    double tmp = radius/2;
    double h = height/2;
    Vector<3> WD1({radius, 0, (-1) * h});
    Vector<3> WD2({tmp, r, (-1) * h});
    Vector<3> WD3({(-1) * tmp, r, (-1) * h});
    Vector<3> WD4({(-1) * radius, 0, (-1) * h});
    Vector<3> WD5({(-1) * tmp, (-1) * r, (-1) * h});
    Vector<3> WD6({tmp, (-1) * r, (-1) * h});
    Vector<3> WG1({radius, 0, h});
    Vector<3> WG2({tmp, r, h});
    Vector<3> WG3({(-1) * tmp, r, h});
    Vector<3> WG4({(-1) * radius, 0, h});
    Vector<3> WG5({(-1) * tmp, (-1) * r, h});
    Vector<3> WG6({tmp, (-1) * r, h});
    std::vector<drawNS::Point3D> top;
    std::vector<drawNS::Point3D> bottom;
    top = {convert(point_in_predecessor(WD1)), convert(point_in_predecessor(WD2)),
           convert(point_in_predecessor(WD3)), convert(point_in_predecessor(WD4)),
           convert(point_in_predecessor(WD5)), convert(point_in_predecessor(WD6)), convert(point_in_predecessor(WD1))};
    bottom = {convert(point_in_predecessor(WG1)), convert(point_in_predecessor(WG2)),
            convert(point_in_predecessor(WG3)), convert(point_in_predecessor(WG4)),
            convert(point_in_predecessor(WG5)), convert(point_in_predecessor(WG6)), convert(point_in_predecessor(WG1))};
    std::vector<std::vector<drawNS::Point3D>> final;
    final.push_back(top);
    final.push_back(bottom);
    if (id == -1)
        id = api->draw_polyhedron(final, "black");
    else {
        id2 = id;
        id = api->draw_polyhedron(final, "black");
    }
}


void Prism_Hex::remove() {
    if (id2 != -1) {
        api->erase_shape(id2);
        id2 = -1;
    }
    else {
        api->erase_shape(id);
        id = -1;
    }
}
