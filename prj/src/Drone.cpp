#include "Drone.hh"

void Drone::fly_forward(const double &dist, drawNS::APIopenGL3D * api) {
    Vector<3> distance({dist,0,0});
    translation(orientation*distance);
    id = 0; //allows to redraw
    draw();
    remove();
}


void Drone::fly_vertically(const double &alt, drawNS::APIopenGL3D * api) {
    Vector<3> altitude({0,0,alt});
    centre = centre + altitude;
    id = 0;  //allows to redraw
    draw();
    remove();
}

void Drone::rotate(const double &angle, drawNS::APIopenGL3D * api) {
    Rotation_Matrix<3> rot_matr(angle);
    orientation = orientation * rot_matr;
    id = 0;  //allows to redraw
    draw();
    remove();
}

void Drone::draw() {
    if (id == 0 || !id_drawn) {
        for (const auto &i : collection)
            i->draw();
        id = -1;
        id_drawn = true;
    }
}

void Drone::remove() {
    for (const auto &i : collection)
        i->remove();
}

void Drone::move_rotors(){
    Rotation_Matrix<3> rot(15);
    for (int i = 0; i < 4; ++i)
        rotors[i].rotation(rot);
}

std::shared_ptr<Drone> Drone::add_shape(const Vector<3> &ctr, const Rotation_Matrix<3> &ori, drawNS::APIopenGL3D *_api) {
    std::shared_ptr<Drone> temp = std::make_shared<Drone>(ctr,ori,_api);
    return temp;
}

bool Drone::if_above(const std::shared_ptr<Drone_Interface> &D) {
    Vector<3> drone_centre = std::dynamic_pointer_cast<Drawing_Interface>(D)->centre_position();
    double horizontal_dist = sqrt(pow(centre[0]-drone_centre[0],2)+pow(centre[1]-drone_centre[1],2));
    if(horizontal_dist <= this->get_R()+std::dynamic_pointer_cast<Scenery_Element_Interface>(D)->get_R())
        return true;
    else
        return false;
}
