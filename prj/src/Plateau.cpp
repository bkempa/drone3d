#include "Plateau.hh"

void Plateau::draw() {
    if (id == -1)
        id = api->draw_polyhedron(vertices, "orange");
}


void Plateau::remove() {
    api->erase_shape(id);
}

std::shared_ptr<Plateau> Plateau::add_shape(const Vector<3> &centre, const double &height, const double &r_min, const double &r_max, drawNS::APIopenGL3D * _api) {
    std::shared_ptr<Plateau> temp = std::make_shared<Plateau>(centre,height,r_min,r_max,_api);
    return temp;
}

bool Plateau::if_above(const std::shared_ptr<Drone_Interface> &D) {
    Vector<3> drone_centre = std::dynamic_pointer_cast<Drawing_Interface>(D)->centre_position();
    double horizontal_dist = sqrt(pow(centre[0]-drone_centre[0],2) + pow(centre[1]-drone_centre[1],2));
    if(horizontal_dist <= Rmax + std::dynamic_pointer_cast<Scenery_Element_Interface>(D)->get_R())
        return true;
    else
        return false;
}

bool Plateau::if_land(const std::shared_ptr<Drone_Interface> &D, double &alt) {
    Vector<3> drone_centre = std::dynamic_pointer_cast<Drawing_Interface>(D)->centre_position();
    double horizontal_dist = sqrt(pow(centre[0]-drone_centre[0],2)+pow(centre[1]-drone_centre[1],2));
    double R_land = Rmin - std::dynamic_pointer_cast<Scenery_Element_Interface>(D)->get_R();
    if(horizontal_dist <= R_land){
        alt = drone_centre[2] - height - 1;
        return true;
    }
    else
        return false;
}
