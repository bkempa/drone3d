#ifndef RECT_CUBOID_HH
#define RECT_CUBOID_HH

/*!
*   \file   
*   \brief Rect_Cuboid class definition.
*
*   The file contains Rect_Cuboid class definition.
*/

#include <iostream>
#include "Coordinate_System.hh"
#include "Vector.hh"
#include "Draw3D_api_interface.hh"
#include "OpenGL_API.hh"
#include "Drawing_Interface.hh"

/*!
*   \brief Class specifying rectangular cuboid concept.
*/
class Rect_Cuboid : public Coordinate_System, public Drawing_Interface {
    /*!
    *   \brief Rectangular cuboid's height.
    */
    double height;
    /*!
    *   \brief Rectangular cuboid's width.
    */
    double width;
    /*!
    *   \brief Rectangular cuboid's depth.
    */
    double depth;
    /*!
    *   \brief Container of points that are cuboid's vertices.
    */
    std::vector<drawNS::Point3D> vertices;
public:
    /*!
    *   \brief Non-parametric constructor of the Rect_Cuboid class.
    */
    Rect_Cuboid() {};
    /*!
    *   \brief Parametric constructor of the Rect_Cuboid class.
    *
    *   \param[in] ctr vector describing cuboid's centre location.
    *   \param[in] ori rotation matrix of cuboid's orientation in terms of Z-axis.
    *   \param[in] pred pointer on coordinate system that is a predecessor of the cuboid's local system.
    *   \param[in] h cuboid's height.
    *   \param[in] w cuboid's width.
    *   \param[in] d cuboid's depth.
    *   \param[in] _api API pointer.
    */
    explicit Rect_Cuboid(Vector<3> ctr, Rotation_Matrix<3> ori, Coordinate_System * pred, const double & h, const double & w, const double & d, drawNS::APIopenGL3D *_api) :
                    Coordinate_System(ctr,ori,pred), height(h), width(w), depth(d) {api = _api;};
    void draw() override;
    void remove() override;
    Vector<3> centre_position() override {return centre;};
    std::vector<drawNS::Point3D> *get_vertices() {return &vertices;};
};


/*!
*   \brief Class specifying rectangular cuboid concept used as drone's body.
*/
class Rect_Cuboid_Drone : public Coordinate_System, public Drawing_Interface {
    /*!
    *   \brief Cuboid's height.
    */
    double height;
    /*!
    *   \brief Cuboid's width.
    */
    double width;
    /*!
    *   \brief Cuboid's depth.
    */
    double depth;
    /*!
    *   \brief Cuboid's ID.
    */
    int id2 = -1;
public:
    /*!
    *   \brief Non-parametric constructor of the Rect_Cuboid_Drone class.
    */
    Rect_Cuboid_Drone() {};
    /*!
    *   \brief Parametric constructor of the Rect_Cuboid class.
    *
    *   \param[in] ctr vector describing cuboid's centre location.
    *   \param[in] ori rotation matrix of cuboid's orientation in terms of Z-axis.
    *   \param[in] pred pointer on coordinate system that is a predecessor of the cuboid's local system.
    *   \param[in] h cuboid's height.
    *   \param[in] w cuboid's width.
    *   \param[in] d cuboid's depth.
    *   \param[in] _api API pointer.
    */
    explicit Rect_Cuboid_Drone(Vector<3> ctr, Rotation_Matrix<3> ori, Coordinate_System * pred, const double & h, const double & w, const double & d,drawNS::APIopenGL3D *_api) :
            Coordinate_System(ctr,ori,pred), height(h), width(w), depth(d) {api=_api;};
    void draw() override;
    void remove() override;
    Vector<3> centre_position() override {return centre;};
};


#endif