#ifndef SCENE_HH
#define SCENE_HH

/*!
*   \file   
*   \brief Scene class definition.
*
*   The file contains Scene class definition.
*/

#include <iostream>
#include <chrono>
#include <thread>
#include <vector>
#include <algorithm>
#include "Hill.hh"
#include "Plateau.hh"
#include "Plateau_Rect.hh"
#include "Drone.hh"

/*!
*   \brief Class specifying scene concept.
*
*   It specifies a scene, that contains all drones and scenery's elements.
*/
class Scene {
    private:
        /*!
        *   \brief API pointer.
        */
        drawNS::APIopenGL3D *api;
        /*!
        *   \brief Container of shared_pointers pointing on drone interface, so on all existing drones.
        */
        std::vector<std::shared_ptr<Drone_Interface>> drones;
        /*!
        *   \brief Container of shared_pointers pointing on scenery element interface, so on all existing obstructions on the surface.
        */
        std::vector<std::shared_ptr<Scenery_Element_Interface>> scenery_elements;
        /*!
        *   \brief Container of shared_pointers pointing on drawing interface, so on all drawable elements.
        */
        std::vector<std::shared_ptr<Drawing_Interface>> drawable_elements;
        /*!
        *   \brief Shared_pointer pointing on drone interface, so the currently chosen drone.
        */
        std::shared_ptr<Drone_Interface> current;
    public:
        /*!
        *   \brief Constructor of the Scene class.
        *   
        *   \param[in] _api API pointer. 
        */
        Scene(drawNS::APIopenGL3D *_api);

        /*!
        *   \brief Metoda powodujaca iluzje plynnego ruchu drona.
        *   
        *   \param[in] altitude value describing change of drone's altitude (negative - fly down, positive - up).
        *   \param[in] angle angle (in degrees) that the drone's orientation should be changed.
        *   \param[in] distance distance to cover (negative - fly back, positive - forward).
        */
        void animate(const double &altitude, const double &angle, const double &distance);       

        /*!
        *   \brief Method displaying all drawable elements in the OpenGL window.
        */
        void draw_everything();    

        /*!
        *   \brief Method checking if between a drone and an obstruction occured a collision.
        *   
        *   \param[in] distance distance from the drone to the surface.
        * 
        *   \return Boolean value of checked conditon.
        */
        bool check_collision(double &distance) const;      

        /*!
        *   \brief Method allowing to add a new surface element (obstruction).
        *   
        *   \param[in] obstruction symbol of an obstruction that the user wants to create on the surface.
        */
        void add_surface_element(const char &obstruction);       

        /*!
        *   \brief Method allowing to remove a chosen surface element (obstruction).
        *   
        *   \param[in] id ID of chosen to remove surface element (obstruction).
        */
        void remove_surface_element(const uint &id);       

        /*!
        *   \brief Metoda displaying indexes and centre coordinates of all scenery elements.
        */
        void display_scenery_elements() const;
        
        /*!
        *   \brief Method allowing to add a new drone.
        *
        *   \param[in] drone_centre vector describing new drone's centre location.
        *   \param[in] rot_mat rotation matrix of angle describing new drone's orientation in terms of Z-axis.
        *   \param[in] api API pointer.
        */
        void add_drone(Vector<3> &drone_centre, const Rotation_Matrix<3> &rot_mat, drawNS::APIopenGL3D * api);
        
        /*!
        *   \brief Method allowing to remove a chosen drone.
        *   
        *   \param[in] id ID of drone chosen to remove.
        */
        void remove_drone(const uint &id);
        
        /*!
        *   \brief Metoda displaying indexes and centre coordinates of all drones.
        */
        void display_drones() const;
        
        /*!
        *   \brief Method allowing to choose a currently controlled drone.
        *
        *   \param[in] id ID of the drone that the user wants to control.
        */
        void choose_drone(const uint &id);
};


#endif