#ifndef DRONE_HH
#define DRONE_HH

/*!
*   \file   
*   \brief Drone class definition.
*
*   The file contains Drone class definition.
*/

#include <iostream>
#include <array>
#include "Coordinate_System.hh"
#include "Rect_Cuboid.hh"
#include "Prism_Hex.hh"
#include "Rotation_Matrix.hh"
#include "Drone_Interface.hh"
#include "Drawing_Interface.hh"
#include "Scenery_Element_Interface.hh"


/*!
*   \brief Class specifying drone concept.
*/
class Drone : protected Coordinate_System, public Drawing_Interface, public Drone_Interface, public Scenery_Element_Interface {
    /*!
    *   \brief Drone's cuboid body.
    */
    Rect_Cuboid_Drone body;
    /*!
    *   \brief Array of 4 hexagonal prisms symbolizing drone's rotors.
    */
    std::array<Prism_Hex, 4> rotors;
    /*!
    *   \brief Boolean value informing if the drone has already been drawn.
    */
    bool id_drawn = false;
    /*!
    *   \brief Container of pointers on drawing interface (all drone's parts).
    */
    std::vector<Drawing_Interface*> collection;
public:
    /*!
    *   \brief Non-parametric constructor of the Drone class.
    */
    Drone() {};
    /*!
    *   \brief Parametric constructor of the Drone class.
    *
    *   \param[in] ctr vector describing drone's centre location.
    *   \param[in] ori rotation matrix of angle describing drone's orientation in terms of Z-axis.
    *   \param[in] _api API pointer.
    */
    explicit Drone(Vector<3> ctr, Rotation_Matrix<3> ori, drawNS::APIopenGL3D *_api) : Coordinate_System(ctr,ori,nullptr), body(Vector<3>({0,0,0}), Rotation_Matrix<3>(), this, 2, 4, 3, _api),
        rotors({
            Prism_Hex(Vector<3>({-2,-2,1}), Rotation_Matrix<3>(), this, 1.8, 0.7, _api),Prism_Hex(Vector<3>({2,-2,1}), Rotation_Matrix<3>(), this, 1.8, 0.7, _api),
            Prism_Hex(Vector<3>({2,2,1}), Rotation_Matrix<3>(), this, 1.8, 0.7, _api),Prism_Hex(Vector<3>({-2,2,1}), Rotation_Matrix<3>(), this, 1.8, 0.7, _api)})
        {api=_api;add_to_collection();R=2.5;};
    /*!
    *   \brief Method adding all drone's parts to the collection.
    */
    void add_to_collection() {
        collection.push_back(&body);
        collection.push_back(&rotors[0]);
        collection.push_back(&rotors[1]);
        collection.push_back(&rotors[2]);
        collection.push_back(&rotors[3]);
    }
    void fly_forward(const double &dist, drawNS::APIopenGL3D * api) override;
    void fly_vertically(const double &alt, drawNS::APIopenGL3D * api) override;
    void rotate(const double &angle, drawNS::APIopenGL3D * api) override;
    void draw() override;
    void remove() override;
    void move_rotors() override;
    Vector<3> centre_position() override {return centre;}
    /*!
        *   \brief Method allowing to add a new drone.
        *
        *   \param[in] ctr vector describing new drone's centre location.
        *   \param[in] ori rotation matrix of angle describing new drone's orientation in terms of Z-axis.
        *   \param[in] _api API pointer.
        * 
        *   \return Shared_pointer on Drone-type element.
        */
    std::shared_ptr<Drone> add_shape(const Vector<3> &ctr, const Rotation_Matrix<3> &ori, drawNS::APIopenGL3D *_api);
    bool if_above(const std::shared_ptr<Drone_Interface> &D) override;
    bool if_land(const std::shared_ptr<Drone_Interface> &D, double &altitude) override {return false;};
};

#endif