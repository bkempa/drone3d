#ifndef COORDINATE_SYSTEM_HH
#define COORDINATE_SYSTEM_HH

/*!
*   \file   
*   \brief Coordinate_System class definition.
*
*   The file contains Coordinate_System class definition.
*/

#include <iostream>
#include "Vector.hh"
#include "Rotation_Matrix.hh"

/*!
*   \brief Class specifying coordinate system concept.
*/
class Coordinate_System {
protected:
    /*!
    *   \brief Pointer on coordinate system that is a predecessor of the specific system.
    */
    Coordinate_System *predecessor;
    /*!
    *   \brief Three dimensional vector describing centre's position of the specific system.
    */
    Vector<3> centre;
    /*!
    *   \brief Rotation matrix describing orientation of the specific system.
    */
    Rotation_Matrix<3> orientation;
public:
    /*!
    *   \brief Non-parametric constructor of the Coordinate_System class.
    */
    Coordinate_System() {};
    /*!
    *   \brief Parametric constructor of the Coordinate_System class.
    *
    *   \param[in]  ctr vector describing centre's position of the constructed system.
    *   \param[in]  ori orientation of the constructed system.
    *   \param[in]  pred pointer on coordinate system that is a predecessor of the constructed system.
    */
    explicit Coordinate_System(Vector<3> ctr, Rotation_Matrix<3> ori, Coordinate_System * pred) :
        predecessor(pred), centre(ctr), orientation(ori) {};
    /*!
    *   \brief Method allowing to shift coordinate system by provided vector.
    *
    *   \param[in] vec vector describing the desired shift of the system.
    */
    void translation(const Vector<3> &vec) {centre = centre + vec;};
    /*!
    *   \brief Method allowing to rotate coordinate system by provided vector.
    *
    *   \param[in] mat rotation matrix of desired angle.
    */
    void rotation(const Rotation_Matrix<3> &mat) {orientation = orientation * mat;};
    /*!
    *   \brief Method allowing to convert a given vector to a vector in terms of the predecessor coordinate system.
    *
    *   \param[in] point vector to be converted.
    * 
    *   \return Vector that is an interpretation of the vector provided as argument in terms of the predecessor coordinate system.
    */
    Vector<3> point_in_predecessor(const Vector<3> &point) const;
    /*!
    *   \brief Method allowing to calculate the coordinate system to the global one.
    * 
    *   \return Coordinate system that is a the same system, but presented in terms of the global system.
    */
    Coordinate_System system_in_global() const;
    /*!
    *   \brief Method allowing to convert a vector to a point.
    *
    *   \param[in] vec vector that we want to convert.
    * 
    *   \return Point that is a result of the conversion.
    */
    drawNS::Point3D convert (Vector<3> vec) {return drawNS::Point3D(vec[0], vec[1], vec[2]);};
};

#endif