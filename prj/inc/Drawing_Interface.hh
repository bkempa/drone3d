#ifndef DRAWING_INTERFACE_HH
#define DRAWING_INTERFACE_HH

/*!
*   \file   
*   \brief Drawing_Interface class definition.
*
*   The file contains Drawing_Interface class definition.
*/

/*!
*   \brief Class specifying drawing interface concept.
*/
class Drawing_Interface {
    protected:
        /*!
        *   \brief API pointer.
        */
        drawNS::APIopenGL3D * api;
        /*!
        *   \brief Drawable element's ID.
        */
        int id = -1;
    public:
        /*!
        *   \brief Method allowing to draw drones and scenery elements.
        */
        virtual void draw() = 0;
        /*!
        *   \brief Method allowing to remove drones and scenery elements.
        */
        virtual void remove() = 0;
        /*!
        *   \brief Method giving access to the vector describing drone's or scenery element's centre position.
        *
        *   \return Vector describing drone's or scenery element's centre position.
        */
        virtual Vector<3> centre_position() = 0;
};

#endif