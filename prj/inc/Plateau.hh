#ifndef PLATEAU_HH
#define PLATEAU_HH

/*!
*   \file   
*   \brief Plateau class definition.
*
*   The file contains Plateau class definition.
*/

#include <iostream>
#include <vector>
#include <random>
#include "Draw3D_api_interface.hh"
#include "OpenGL_API.hh"
#include "Coordinate_System.hh"
#include "Scenery_Element_Interface.hh"
#include "Drawing_Interface.hh"

/*!
*   \brief Class specifying plateau concept.
*/
class Plateau : public Coordinate_System, public Scenery_Element_Interface, public Drawing_Interface {
    /*!
    *   \brief Container of containers of points that are plateau's vertices.
    */
    std::vector<std::vector<drawNS::Point3D>> vertices;
    /*!
    *   \brief Plateau's height.
    */
    double height;
    /*!
    *   \brief Minimal distance from the plateau base's centre to the vertex.
    */
    int Rmin;
    /*!
    *   \brief Maximal distance from the plateau base's centre to the vertex.
    */
    int Rmax;
    public:
        bool if_above(const std::shared_ptr<Drone_Interface> &D) override;
        bool if_land(const std::shared_ptr<Drone_Interface> &D, double &alt) override;
        /*!
        *   \brief Non-parametric constructor of the Plateau class.
        */
        Plateau() {};
        /*!
        *   \brief Parametric constructor of the Plateau class.
        *
        *   \param[in] centre vector describing plateau's centre location.
        *   \param[in] hgh plateau's height.
        *   \param[in] _Rmin minimal distance from the plateau base's centre to the vertex.
        *   \param[in] _Rmax maximal distance from the plateau base's centre to the vertex.
        *   \param[in] _api API pointer.
        */
        explicit Plateau(Vector<3> centre, double hgh, double _Rmin, double _Rmax, drawNS::APIopenGL3D *_api) :
            Coordinate_System(centre, Rotation_Matrix<3> (0), nullptr), height(hgh), Rmin(_Rmin), Rmax(_Rmax) {
                api = _api;
                std::vector<drawNS::Point3D> temp;
                std::random_device random;
                std::default_random_engine generator(random());
                std::uniform_int_distribution<int> dist_vertices(3,9);
                std::uniform_real_distribution<double> dist_R(Rmin,Rmax);
                int vertices_num = dist_vertices(generator);
                double angles_temp = 360/vertices_num;
                double angle = 0;
                std::vector<double> lengths;
                for (int i = 0; i < vertices_num; ++i){
                    lengths.push_back(dist_R(generator));
                    Vector<3> tmp1({lengths[i],0,-hgh/2});
                    Rotation_Matrix<3> mat(angle);
                    tmp1 = mat*tmp1;
                    temp.push_back(convert(point_in_predecessor(tmp1)));
                    angle += angles_temp;
                }
                vertices.push_back(temp);
                temp.clear();
                angle = 0;
                for (int i = 0; i < vertices_num; ++i){
                    Vector<3> tmp_top({lengths[i],0,hgh/2});
                    Rotation_Matrix<3> mat(angle);
                    tmp_top = mat*tmp_top;
                    temp.push_back(convert(point_in_predecessor(tmp_top)));
                    angle += angles_temp;
                }
                vertices.push_back(temp);
                R = Rmax;
            }
        void draw() override;
        void remove() override;
        Vector<3> centre_position() override {return centre;}
        /*!
        *   \brief Metoda umozliwiajaca dodanie nowego plaskowyzu.
        *
        *   \param[in] centre vector describing new plateau's centre location.
        *   \param[in] height new plateau's height.
        *   \param[in] r_min minimal distance from the new plateau base's centre to the vertex.
        *   \param[in] r_max maximal distance from the new plateau base's centre to the vertex.
        *   \param[in] _api API pointer.
        * 
        *   \return Shared_pointer on Plateau-type element.
        */
        std::shared_ptr<Plateau> add_shape(const Vector<3> &centre, const double &height, const double &r_min, const double &r_max, drawNS::APIopenGL3D * _api);   
};


#endif