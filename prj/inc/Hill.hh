#ifndef HILL_HH
#define HILL_HH

/*!
*   \file   
*   \brief Hill class definition.
*
*   The file contains Hill class definition.
*/

#include <iostream>
#include <cmath>
#include <vector>
#include <random>
#include "Coordinate_System.hh"
#include "Scenery_Element_Interface.hh"
#include "Drone_Interface.hh"
#include "Drawing_Interface.hh"
#include "Draw3D_api_interface.hh"
#include "OpenGL_API.hh"

/*!
*   \brief Class specifying hill concept.
*/
class Hill : public Drawing_Interface, public Scenery_Element_Interface,  public Coordinate_System {
    /*!
    *   \brief Container of containers of points that are hill's vertices.
    */
    std::vector<std::vector<drawNS::Point3D>> vertices;
    /*!
    *   \brief Height of the hill.
    */
    double height;
    /*!
    *   \brief Minimal distance from the hill base's centre to the vertex.
    */
    int Rmin;
    /*!
    *   \brief Maximal distance from the hill base's centre to the vertex.
    */
    int Rmax;
    public:
        bool if_above(const std::shared_ptr<Drone_Interface> &D) override;
        bool if_land(const std::shared_ptr<Drone_Interface> &D, double & alt) override {return false;};
        /*!
        *   \brief Non-parametric constructor of the Hill class.
        */
        Hill() {};
        /*!
        *   \brief Parametric constructor of the Drone class.
        *
        *   \param[in] centre vector describing hill's centre location.
        *   \param[in] hgh height of the hill.
        *   \param[in] _Rmin minimal distance from the hill base's centre to the vertex.
        *   \param[in] _Rmax maximal distance from the hill base's centre to the vertex.
        *   \param[in] _api API pointer.
        */
        explicit Hill(Vector<3> centre, double hgh, double _Rmin, double _Rmax, drawNS::APIopenGL3D *_api) :
            Coordinate_System(centre, Rotation_Matrix<3> (0), nullptr), height(hgh), Rmin(_Rmin), Rmax(_Rmax) {
                api = _api;
                std::vector<drawNS::Point3D> temp;
                std::random_device random;
                std::default_random_engine generator(random());
                std::uniform_int_distribution<int> distribution_wierzch(3,9);
                std::uniform_real_distribution<float> distribution_R(Rmin,Rmax);
                int vertices_num = distribution_wierzch(generator);
                double angles_temp = 360/vertices_num;
                double angle = 0;
                for (int i = 0; i < vertices_num; ++i){
                    double R = distribution_R(generator);
                    Vector<3> tmp1({R,0,-hgh/2});
                    Rotation_Matrix<3> mat(angle);
                    tmp1 =  mat*tmp1;
                    temp.push_back(convert(point_in_predecessor(tmp1)));
                    angle += angles_temp;
                }
                vertices.push_back(temp);
                temp.clear();
                for (int i = 0; i < vertices_num; ++i){
                    temp.push_back(convert(point_in_predecessor(Vector<3>({0,0,hgh/2}))));
                    angle += angles_temp;
                }
                vertices.push_back(temp);
                R = Rmax;
            }
        void draw() override;
        void remove() override;
        Vector<3> centre_position() override {return centre;}
        /*!
        *   \brief Method allowing to add a new hill.
        *
        *   \param[in] centre vector describing new hill's centre location.
        *   \param[in] height height of the new hill.
        *   \param[in] r_min minimal distance from the new hill base's centre to the vertex.
        *   \param[in] r_max maximal distance from the new hill base's centre to the vertex.
        *   \param[in] _api API pointer
        * 
        *   \return Shared_pointer on Hill-type element.
        */
        std::shared_ptr<Hill> add_shape(const Vector<3> &centre, const double &height, const double &r_min, const double &r_max, drawNS::APIopenGL3D * _api);
};

#endif