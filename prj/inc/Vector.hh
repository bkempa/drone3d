#ifndef VECTOR_HH
#define VECTOR_HH

/*!
*   \file   
*   \brief Vector class definition.
*
*   The file contains Vector class definition.
*/

#include <iostream>
#include <array>
#include <cmath>
#include "Draw3D_api_interface.hh"


/*!
*   \brief Class specifying vector concept.
*/
template <int SIZE>
class Vector {
  private:
    /*!
    *   \brief array of values of SIZE-dimensional vector.
    */
    std::array<double, SIZE> arr;
    /*!
    *   \brief Number of currently existing vectors.
    */
    inline static int current_number = 0;
    /*!
    *   \brief Number of all created vectors during program work.
    */
    inline static int number_of_created = 0;
  public:
    /*!
    *   \brief Add operator overloading (responsible for adding two vectors).
    *   
    *   \param[in] arg2 vector that we want to add to another vector.
    * 
    *   \return Vector that is a sum of two vectors.
    */
    Vector<SIZE> operator+(const Vector<SIZE> & arg2) const;
    /*!
    *   \brief Substract operator overloading (responsible for substracting two vectors).
    *
    *   \param[in] arg2 vector that we want to substract from another vector.
    * 
    *   \return Vector that is a difference of two vectors.
    */
    Vector<SIZE> operator-(const Vector<SIZE> & arg2) const;
    /*!
    *   \brief Multiplying operator overloading (responsible for multiplying a vector by a number).
    *
    *   \param[in] arg2 number that the vector is to be multiplied by.
    * 
    *   \return Vector that is a result of multiplication.
    */
    Vector<SIZE> operator*(double arg2) const;
    /*!
    *   \brief Multiplying operator overloading (responsible for multiplying two vectors).
    *
    *   \param[in] arg2 vector that we want to multiply by another vector.
    * 
    *   \return Dot product of two vectors.
    */
    double operator*(const Vector<SIZE> &arg2) const;
    /*!
    *   \brief Method counting vector's length (module).
    * 
    *   \return Vector's length (module).
    */
    double length() const;
    /*!
    *   \brief Parametric constructor of the Vector class.
    *
    *   Creates a new vector and increments number of currently existing and all created vectors.
    * 
    *   \param[in] arg array of values that we want to construct a vector with.
    */
    Vector(std::array<double, SIZE> arg) : arr(arg) {std::copy(arg.begin(), arg.end(), arr.begin()); current_number++; number_of_created++;};
    /*!
    *   \brief Non-parametric constructor of the Vector class.
    *
    *   Creates a new vector and increments number of currently existing and all created vectors.
    */
    Vector() {current_number++; number_of_created++;};
    /*!
    *   \brief Copy constructor of the Vector class.
    *
    *   Copies a vector and increments number of currently existing and all created vectors.
    *
    *   \param[in] vec vector that we want to copy.
    */
    Vector(const Vector &vec) {*this = vec; current_number++; number_of_created++;};
    /*!
    *   \brief Destructor of the Vector class.
    *
    *   Removes a vector and decrements the number of currently existing vectors.
    */
    ~Vector() {current_number--;};
    /*!
    *   \brief Access operator overloading (getter) for a vector.
    *
    *   \param[in] ind - indeks wspolrzednej wektora, ktorej wartosc chcemy uzyskac.
    * 
    *   \return Value on a provided position in a vector.
    */
    const double & operator[](int ind) const;
    /*!
    *   \brief Access operator overloading (setter) for a vector.
    *
    *   \param[in] ind index of position in a vector that we want to get the value from.
    * 
    *   \return Value on a provided position in a vector.
    */
    double & operator[](int ind);
    /*!
    *   \brief Method displaying number of currently existing and total number of created vectors.
    */
    static void display(){
        std::cout << "Current number of vectors: " << current_number << std::endl;
        std::cout << "Number of created vector in total: " << number_of_created << std::endl;
    }
};

/*!
*   \brief Przeciazenie operatora strumienia wejsciowego.
*
*   \param[in] Strm input stream.
*   \param[in] vec vector that we want to enter.
* 
*   \return Input stream.
*/
template <int SIZE>
std::istream& operator >> (std::istream &Strm, Vector<SIZE> &vec);
/*!
*   \brief Output stream operator overloading.
*
*   \param[in] Strm output stream.
*   \param[in] vec vector that we want to display.
* 
*   \return Output stream.
*/
template <int SIZE>
std::ostream& operator << (std::ostream &Strm, const Vector<SIZE> &vec);


#endif