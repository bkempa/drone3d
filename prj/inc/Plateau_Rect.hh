#ifndef PLATEAU_RECT_HH
#define PLATEAU_RECT_HH

/*!
*   \file   
*   \brief Plateau_Rect class definition.
*
*   The file contains Plateau_Rect class definition.
*/

#include "Rect_Cuboid.hh"
#include "Scenery_Element_Interface.hh"
#include <cmath>

/*!
*   \brief Class specifying rectangular plateau concept.
*/
class Plateau_Rect : public Rect_Cuboid, public Scenery_Element_Interface {
    public:
        bool if_above(const std::shared_ptr<Drone_Interface> &D) override;
        bool if_land(const std::shared_ptr<Drone_Interface> &D, double &alt) override;
        /*!
        *   \brief Non-parametric constructor of the Plateau_Rect class.
        */
        Plateau_Rect() {};
        /*!
        *   \brief Parametric constructor of the Plateau_Rect class.
        *
        *   \param[in] ctr vector describing rectangular plateau's centre location.
        *   \param[in] orientation rotation matrix of rectangular plateau's orientation in terms of Z-axis.
        *   \param[in] hgh rectangular plateau's height.
        *   \param[in] width rectangular plateau's width.
        *   \param[in] depth rectangular plateau's depth.
        *   \param[in] _api API pointer.
        */
        explicit Plateau_Rect(Vector<3> ctr,Rotation_Matrix<3> orientation,const double & hgh,const double & width,const double & depth,drawNS::APIopenGL3D *_api) :
            Rect_Cuboid(ctr,orientation,nullptr,hgh,width,depth,_api){R = (double)sqrt(pow(width/2,2)+pow(depth/2,2));};
        Vector<3> centre_position() override {return centre;}
        /*!
        *   \brief Method allowing to add a new rectangular plateau.
        *
        *   \param[in] ctr vector describing new rectangular plateau's centre location.
        *   \param[in] orientation rotation matrix of new rectangular plateau's orientation in terms of Z-axis.
        *   \param[in] hgh new rectangular plateau's height.
        *   \param[in] width new rectangular plateau's width.
        *   \param[in] depth new rectangular plateau's depth.
        *   \param[in] _api API pointer.
        * 
        *   \return Shared_pointer on Plateau_Rect-type element.
        */
        std::shared_ptr<Plateau_Rect> add_shape(const Vector<3> &ctr, const Rotation_Matrix<3> &orientation, const double &hgh, const double &width, const double &depth, drawNS::APIopenGL3D *_api);
};


#endif