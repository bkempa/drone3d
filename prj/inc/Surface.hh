#ifndef SURFACE_HH
#define SURFACE_HH

/*!
*   \file   
*   \brief Surface class definition.
*
*   The file contains Surface class definition.
*/

#include <vector>
#include "OpenGL_API.hh"
#include "Vector.hh"

/*!
*   \brief Class specifying flat surface concept.
*/
class Surface {
    /*!
    *   \brief Altitude on which the surface is located.
    */
    double altitude;
    /*!
    *   \brief Method allowing to convert a vector to a point.
    *
    *   \param[in] vec vector that we want to convert.
    * 
    *   \return Point created from a provided vector.
    */
    drawNS::Point3D convert (Vector<3> vec) {return drawNS::Point3D(vec[0], vec[1], vec[2]);};
public:
    /*!
    *   \brief Parametric contructor of a Surface class.
    *   
    *   \param[in] h altitude on which the surface should be created.
    */
    Surface(double h) : altitude(h) {};
    /*!
    *   \brief Method allowing to draw a surface.
    */
    void draw(drawNS::APIopenGL3D *_api);
};


#endif