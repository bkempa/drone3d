#ifndef SCENERY_ELEMENT_INTERFACE_HH
#define SCENERY_ELEMENT_INTERFACE_HH

#include <memory>
#include "OpenGL_API.hh"
#include "Drone_Interface.hh"

/*!
*   \file   
*   \brief Scenery_Element_Interface class definition.
*
*   The file contains Scenery_Element_Interface class definition.
*/

/*!
*   \brief Class specifying scenery element interface concept.
*/
class Scenery_Element_Interface {
    protected:
        double R = 0;
    public:
        /*!
        *   \brief Method checking if a drone is above an obstruction.
        *
        *   \param[in] D shared_pointer on a drone (interface) which presence above an obstruction is checked.
        * 
        *   \return Boolean value of the checked condition.
        */
        virtual bool if_above(const std::shared_ptr<Drone_Interface> &D) = 0;
        /*!
        *   \brief Method okreslajaca czy dron moze ladowac na danej przeszkodzie.
        *
        *   \param[in] D shared_pointer on a drone (interface) for which a possibility to land is checked.
        *   \param[in] alt altitude for a drone to land.
        * 
        *   \return Boolean value of the checked condition.
        */
        virtual bool if_land(const std::shared_ptr<Drone_Interface> &D, double &alt) = 0;
        /*!
        *   \brief Getter giving access to the R field (the longest radius of scenery element).
        *
        *   \return Length of the longest figure's radius.
        */
        double get_R() const {return R;};
};

#endif