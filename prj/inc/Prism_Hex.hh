#ifndef PRISM_HEX_HH
#define PRISM_HEX_HH

/*!
*   \file   
*   \brief Prism_Hex class definition.
*
*   The file contains Prism_Hex class definition.
*/

#include <cmath>
#include <vector>
#include "Vector.hh"
#include "OpenGL_API.hh"
#include "Coordinate_System.hh"
#include "Drawing_Interface.hh"

/*!
*   \brief Class specifying hexagonal prism concept.
*/
class Prism_Hex : public Coordinate_System, public Drawing_Interface {
    /*!
    *   \brief Length from the prism's base to the vertex.
    */
    double radius;
    /*!
    *   \brief Prism's height.
    */
    double height;
    /*!
    *   \brief Prism's ID.
    */
    int id2 = -1;
public:
    /*!
    *   \brief Non-parametric constructor of the Prism_Hex class.
    */
    Prism_Hex() {};
    /*!
    *   \brief Parametric constructor of the Prism_Hex class.
    *
    *   \param[in] ctr vector describing prism's centre location.
    *   \param[in] ori rotation matrix of prism's orientation in terms of Z-axis.
    *   \param[in] pred pointer on coordinate system that is a predecessor of the prism's local system.
    *   \param[in] r length from the prism's base to the vertex.
    *   \param[in] h prism's height.
    *   \param[in] _api API pointer.
    */
    explicit Prism_Hex(Vector<3> ctr, Rotation_Matrix<3> ori, Coordinate_System * pred, double r, double h, drawNS::APIopenGL3D *_api) :
        Coordinate_System(ctr,ori,pred), radius(r), height(h) {api=_api;};
    void draw() override;
    void remove() override;
    Vector<3> centre_position() override;
};


#endif