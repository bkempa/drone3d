#ifndef DRONE_INTERFACE_HH
#define DRONE_INTERFACE_HH

/*!
*   \file   
*   \brief Drone_Interface class definition.
*
*   The file contains Drone_Interface class definition.
*/

/*!
*   \brief Class specifying drone interface concept.
*/
class Drone_Interface {
    public:
        /*!
        *   \brief Method allowing a drone to fly vertically.
        *
        *   \param[in] alt value describing change of drone's altitude (negative - fly down, positive - up).
        *   \param[in] api API pointer.
        */
        virtual void fly_vertically(const double &alt, drawNS::APIopenGL3D * api) = 0;
        /*!
        *   \brief Method allowing a drone to fly back and forward.
        *
        *   \param[in] dist distance to cover (negative - fly back, positive - forward).
        *   \param[in] api API pointer.
        */
        virtual void fly_forward(const double &dist, drawNS::APIopenGL3D * api) = 0;
        /*!
        *   \brief Metoda allowing a drone to rotate around Z-axis.
        *
        *   \param[in] angle angle (in degrees) that the drone's orientation should be changed.
        *   \param[in] api API pointer.
        */
        virtual void rotate(const double &angle, drawNS::APIopenGL3D * api) = 0;
        /*!
        *   \brief Method making an illusion of drone rotors' rotational move.
        */
        virtual void move_rotors() = 0;
};

#endif