#ifndef ROTATION_MATRIX_HH
#define ROTATION_MATRIX_HH

/*!
*   \file   
*   \brief Rotation_Matrix class definition.
*
*   The file contains Rotation_Matrix class definition.
*/

#include <iostream>
#include <array>
#include <cmath>
#include "Vector.hh"

/*!
*   \brief Class specifying rotation matrix concept.
*/
template <int SIZE>
class Rotation_Matrix {
  private:
    /*!
    *   \brief Array of matrix' values.
    */
    std::array<Vector<SIZE>, SIZE> matrix;
  public:
    /*!
    *   \brief Parametric constructor of the Rotation_Matrix class.
    *
    *   \param[in] angle angle (in degrees) for which a rotation matrix is to be created.
    *   \param[in] axis - axisfor which a rotation matrix is to be created (X, Y or Z).
    */
    explicit Rotation_Matrix (double angle = 0, char axis = 'Z');
    /*!
    *   \brief Multiply operator overloading (responsible for multiplying matrix by vector).
    *
    *   \param[in] arg2 vector that a matrix should be multiplied by.
    * 
    *   \return Vector that is a result of multiplying.
    */
    Vector<SIZE> operator*(const Vector<SIZE> & arg2) const;
    /*!
    *   \brief Multiply operator overloading (responsible for multiplying two matrices).
    *
    *   \param[in] arg2 matrix that an another matrix should be multiplied by.
    * 
    *   \return Matrix that is a result of multiplying.
    */
    Rotation_Matrix<SIZE> operator*(const Rotation_Matrix<SIZE> & arg2) const;
    /*!
    *   \brief Access operator overloading (getter) for a matrix.
    *
    *   \param[in] ind index of position in a matrix that we want to get the value from.
    * 
    *   \return Value in on a provided position in a matrix.
    */
    const Vector<SIZE> & operator[] (const int &ind) const;
};

/*!
*   \brief Output stream operator overloading.
*
*   \param[in] Strm output stream.
*   \param[in] Mat matrix that we want to display.
* 
*   \return Output stream.
*/
template <int SIZE>
std::ostream& operator << (std::ostream &Strm, const Rotation_Matrix<SIZE> &Mat);


#endif