var files_dup =
[
    [ "Coordinate_System.cpp", "_coordinate___system_8cpp.html", null ],
    [ "Coordinate_System.hh", "_coordinate___system_8hh.html", [
      [ "Coordinate_System", "class_coordinate___system.html", "class_coordinate___system" ]
    ] ],
    [ "Draw3D_api_interface.hh", "_draw3_d__api__interface_8hh.html", [
      [ "Point3D", "classdraw_n_s_1_1_point3_d.html", "classdraw_n_s_1_1_point3_d" ],
      [ "Draw3DAPI", "classdraw_n_s_1_1_draw3_d_a_p_i.html", "classdraw_n_s_1_1_draw3_d_a_p_i" ]
    ] ],
    [ "Drawing_Interface.hh", "_drawing___interface_8hh.html", [
      [ "Drawing_Interface", "class_drawing___interface.html", "class_drawing___interface" ]
    ] ],
    [ "Drone.cpp", "_drone_8cpp.html", null ],
    [ "Drone.hh", "_drone_8hh.html", [
      [ "Drone", "class_drone.html", "class_drone" ]
    ] ],
    [ "Drone_Interface.hh", "_drone___interface_8hh.html", [
      [ "Drone_Interface", "class_drone___interface.html", "class_drone___interface" ]
    ] ],
    [ "Hill.cpp", "_hill_8cpp.html", null ],
    [ "Hill.hh", "_hill_8hh.html", [
      [ "Hill", "class_hill.html", "class_hill" ]
    ] ],
    [ "main.cpp", "main_8cpp.html", "main_8cpp" ],
    [ "OpenGL_API.cpp", "_open_g_l___a_p_i_8cpp.html", "_open_g_l___a_p_i_8cpp" ],
    [ "OpenGL_API.hh", "_open_g_l___a_p_i_8hh.html", [
      [ "data", "structdraw_n_s_1_1data.html", "structdraw_n_s_1_1data" ],
      [ "APIopenGL3D", "classdraw_n_s_1_1_a_p_iopen_g_l3_d.html", "classdraw_n_s_1_1_a_p_iopen_g_l3_d" ]
    ] ],
    [ "Plateau.cpp", "_plateau_8cpp.html", null ],
    [ "Plateau.hh", "_plateau_8hh.html", [
      [ "Plateau", "class_plateau.html", "class_plateau" ]
    ] ],
    [ "Plateau_Rect.cpp", "_plateau___rect_8cpp.html", null ],
    [ "Plateau_Rect.hh", "_plateau___rect_8hh.html", [
      [ "Plateau_Rect", "class_plateau___rect.html", "class_plateau___rect" ]
    ] ],
    [ "Prism_Hex.cpp", "_prism___hex_8cpp.html", null ],
    [ "Prism_Hex.hh", "_prism___hex_8hh.html", [
      [ "Prism_Hex", "class_prism___hex.html", "class_prism___hex" ]
    ] ],
    [ "Rect_Cuboid.cpp", "_rect___cuboid_8cpp.html", null ],
    [ "Rect_Cuboid.hh", "_rect___cuboid_8hh.html", [
      [ "Rect_Cuboid", "class_rect___cuboid.html", "class_rect___cuboid" ],
      [ "Rect_Cuboid_Drone", "class_rect___cuboid___drone.html", "class_rect___cuboid___drone" ]
    ] ],
    [ "Rotation_Matrix.cpp", "_rotation___matrix_8cpp.html", "_rotation___matrix_8cpp" ],
    [ "Rotation_Matrix.hh", "_rotation___matrix_8hh.html", "_rotation___matrix_8hh" ],
    [ "Scene.cpp", "_scene_8cpp.html", null ],
    [ "Scene.hh", "_scene_8hh.html", [
      [ "Scene", "class_scene.html", "class_scene" ]
    ] ],
    [ "Scenery_Element_Interface.hh", "_scenery___element___interface_8hh.html", [
      [ "Scenery_Element_Interface", "class_scenery___element___interface.html", "class_scenery___element___interface" ]
    ] ],
    [ "Surface.cpp", "_surface_8cpp.html", null ],
    [ "Surface.hh", "_surface_8hh.html", [
      [ "Surface", "class_surface.html", "class_surface" ]
    ] ],
    [ "Vector.cpp", "_vector_8cpp.html", "_vector_8cpp" ],
    [ "Vector.hh", "_vector_8hh.html", "_vector_8hh" ]
];