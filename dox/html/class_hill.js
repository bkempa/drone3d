var class_hill =
[
    [ "Hill", "class_hill.html#a2910762b8280592802031e8e67feb7bd", null ],
    [ "Hill", "class_hill.html#a7963846e81490a1c55f6840853c8a5c7", null ],
    [ "add_shape", "class_hill.html#a3ac8deb3928f0e8438f5af1b36f0a81e", null ],
    [ "centre_position", "class_hill.html#a8a682d31f14f2816e03cbe0bb17459c2", null ],
    [ "draw", "class_hill.html#a32c7f850297d55909cd6b82e55df6a7c", null ],
    [ "if_above", "class_hill.html#ae3f11db813b4a22828cabc8798016a4c", null ],
    [ "if_land", "class_hill.html#adab3b96fb57e1aae32d0ff6c57d07388", null ],
    [ "remove", "class_hill.html#a0b13ecfe66381b6459e649a22968a3f4", null ],
    [ "height", "class_hill.html#ac337fa2e721aad8a1b7f408b07c04d89", null ],
    [ "Rmax", "class_hill.html#aa744fbf0e29e1dfd2b222e40b9f92cee", null ],
    [ "Rmin", "class_hill.html#ae44660039f8a7131f61d7b934b11c29a", null ],
    [ "vertices", "class_hill.html#a2c35ce64751591dfc0eaf46978113a05", null ]
];