var class_coordinate___system =
[
    [ "Coordinate_System", "class_coordinate___system.html#afdbcec8d7f4042bf138cce6000820a17", null ],
    [ "Coordinate_System", "class_coordinate___system.html#a775c9fbca193090cf0becea4699de862", null ],
    [ "convert", "class_coordinate___system.html#aee103c6674a132dea0dc5e172e34bb4c", null ],
    [ "point_in_predecessor", "class_coordinate___system.html#a04b6995374b9527bb9c40e8c23236451", null ],
    [ "rotation", "class_coordinate___system.html#ae0ddb7aaed1faf1234300a38dc30d719", null ],
    [ "system_in_global", "class_coordinate___system.html#a375ac706cf715d0370f0cbbf1d24996c", null ],
    [ "translation", "class_coordinate___system.html#aabeb4ff516bc6b3a4d2771ef4514c587", null ],
    [ "centre", "class_coordinate___system.html#a490687d32cd8e9e71800e0cfaad5c369", null ],
    [ "orientation", "class_coordinate___system.html#a4c02c2dcf30ca9489d480fc02a0c10d7", null ],
    [ "predecessor", "class_coordinate___system.html#ab24a5fb24b3de369193e27423dbb07fb", null ]
];