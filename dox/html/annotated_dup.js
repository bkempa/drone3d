var annotated_dup =
[
    [ "drawNS", "namespacedraw_n_s.html", "namespacedraw_n_s" ],
    [ "Coordinate_System", "class_coordinate___system.html", "class_coordinate___system" ],
    [ "Drawing_Interface", "class_drawing___interface.html", "class_drawing___interface" ],
    [ "Drone", "class_drone.html", "class_drone" ],
    [ "Drone_Interface", "class_drone___interface.html", "class_drone___interface" ],
    [ "Hill", "class_hill.html", "class_hill" ],
    [ "Plateau", "class_plateau.html", "class_plateau" ],
    [ "Plateau_Rect", "class_plateau___rect.html", "class_plateau___rect" ],
    [ "Prism_Hex", "class_prism___hex.html", "class_prism___hex" ],
    [ "Rect_Cuboid", "class_rect___cuboid.html", "class_rect___cuboid" ],
    [ "Rect_Cuboid_Drone", "class_rect___cuboid___drone.html", "class_rect___cuboid___drone" ],
    [ "Rotation_Matrix", "class_rotation___matrix.html", "class_rotation___matrix" ],
    [ "Scene", "class_scene.html", "class_scene" ],
    [ "Scenery_Element_Interface", "class_scenery___element___interface.html", "class_scenery___element___interface" ],
    [ "Surface", "class_surface.html", "class_surface" ],
    [ "Vector", "class_vector.html", "class_vector" ]
];