var class_drone =
[
    [ "Drone", "class_drone.html#ab692baa4be5c43b72990ce1b01bdc805", null ],
    [ "Drone", "class_drone.html#ad9a4374499b99849901e5f04f57e96f2", null ],
    [ "add_shape", "class_drone.html#acaae6bcda470f3788be8e92a201b2efd", null ],
    [ "add_to_collection", "class_drone.html#a9d7798a35ef69fae4d6025ec4b51663b", null ],
    [ "centre_position", "class_drone.html#afb43eb734ac91040c9b095e70ba72d68", null ],
    [ "draw", "class_drone.html#a07a48ff3be8ab7fc7ef56bb222bce74b", null ],
    [ "fly_forward", "class_drone.html#ab6e969ffe185f99ba50c3db1b43b390f", null ],
    [ "fly_vertically", "class_drone.html#a1c830495da6fd66cdd5448550668ce76", null ],
    [ "if_above", "class_drone.html#a3dd7dc1880bbd951554f34f55bbd87fb", null ],
    [ "if_land", "class_drone.html#a80ac74de4b9690ac4a595d7338e22564", null ],
    [ "move_rotors", "class_drone.html#aba58da37297856efb956afed8cef4acc", null ],
    [ "remove", "class_drone.html#ad336809b1f83a5a148860f5ef7b35808", null ],
    [ "rotate", "class_drone.html#a065aab8f2341b39e80e17aa385105022", null ],
    [ "body", "class_drone.html#ac5dfe137621f1ad6e7abd1091c5671c4", null ],
    [ "collection", "class_drone.html#a433219f7fe7e44938d207acd00d1c8ab", null ],
    [ "id_drawn", "class_drone.html#a3c776074a4d546a46b8a8f8cf2dafaa0", null ],
    [ "rotors", "class_drone.html#a9a5aed956fa92e79c7a1618acd5a7e81", null ]
];