var class_rotation___matrix =
[
    [ "Rotation_Matrix", "class_rotation___matrix.html#aea3b87a70186c9bafcb158dabbdf472e", null ],
    [ "Rotation_Matrix", "class_rotation___matrix.html#a773f427dbe6e8b46e8ffa1b01d32c775", null ],
    [ "operator*", "class_rotation___matrix.html#a1960eea5758e8b0ab659ff774f8b838c", null ],
    [ "operator*", "class_rotation___matrix.html#a37374fd2918d1bea7b5bc51e0d2cbe31", null ],
    [ "operator[]", "class_rotation___matrix.html#a1150b982afec656bdfcad65f5d9d6761", null ],
    [ "matrix", "class_rotation___matrix.html#a61cc586449d36a6eed59597b38429a6a", null ]
];