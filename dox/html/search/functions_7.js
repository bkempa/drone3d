var searchData=
[
  ['if_5fabove_263',['if_above',['../class_drone.html#a3dd7dc1880bbd951554f34f55bbd87fb',1,'Drone::if_above()'],['../class_hill.html#ae3f11db813b4a22828cabc8798016a4c',1,'Hill::if_above()'],['../class_plateau.html#a9d869c8b9f3701bf510c9b0af45555c5',1,'Plateau::if_above()'],['../class_plateau___rect.html#a619c3c9f3d5eea593503fedc2dd65d85',1,'Plateau_Rect::if_above()'],['../class_scenery___element___interface.html#a03ef65244a56acb9b894e3c974813009',1,'Scenery_Element_Interface::if_above()']]],
  ['if_5fland_264',['if_land',['../class_drone.html#a80ac74de4b9690ac4a595d7338e22564',1,'Drone::if_land()'],['../class_hill.html#adab3b96fb57e1aae32d0ff6c57d07388',1,'Hill::if_land()'],['../class_plateau.html#a97a06df4d760ef990daf13a8d8e7c625',1,'Plateau::if_land()'],['../class_plateau___rect.html#ac9ad2b6269b76af12cb4116ecde5058a',1,'Plateau_Rect::if_land()'],['../class_scenery___element___interface.html#a90012e4175329119cdcb48b1c9912388',1,'Scenery_Element_Interface::if_land()']]],
  ['initialize_265',['Initialize',['../classdraw_n_s_1_1_a_p_iopen_g_l3_d.html#a560312018964189280465be188662065',1,'drawNS::APIopenGL3D']]]
];
