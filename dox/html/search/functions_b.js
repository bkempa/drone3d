var searchData=
[
  ['plateau_279',['Plateau',['../class_plateau.html#a07026170529dd928238ee45de0a797d7',1,'Plateau::Plateau()'],['../class_plateau.html#a3c73ba709d9ce0dbd824dd944d820235',1,'Plateau::Plateau(Vector&lt; 3 &gt; centre, double hgh, double _Rmin, double _Rmax, drawNS::APIopenGL3D *_api)']]],
  ['plateau_5frect_280',['Plateau_Rect',['../class_plateau___rect.html#ad5d8883cd8dd74d8121b40014956a873',1,'Plateau_Rect::Plateau_Rect()'],['../class_plateau___rect.html#a2dc202df6f6df30ca1d5bd6c139f5b3e',1,'Plateau_Rect::Plateau_Rect(Vector&lt; 3 &gt; ctr, Rotation_Matrix&lt; 3 &gt; orientation, const double &amp;hgh, const double &amp;width, const double &amp;depth, drawNS::APIopenGL3D *_api)']]],
  ['point3d_281',['Point3D',['../classdraw_n_s_1_1_point3_d.html#a0c903a94653375c05122ac5cc73dcf39',1,'drawNS::Point3D::Point3D()=delete'],['../classdraw_n_s_1_1_point3_d.html#a01dac6d46c79850baf2503751974b63b',1,'drawNS::Point3D::Point3D(double x, double y, double z)']]],
  ['point_5fin_5fpredecessor_282',['point_in_predecessor',['../class_coordinate___system.html#a04b6995374b9527bb9c40e8c23236451',1,'Coordinate_System']]],
  ['prism_5fhex_283',['Prism_Hex',['../class_prism___hex.html#a0ed82edd892ae5d5dddc54f291e31922',1,'Prism_Hex::Prism_Hex()'],['../class_prism___hex.html#ae464163ee1b696abf847a61b72dbe457',1,'Prism_Hex::Prism_Hex(Vector&lt; 3 &gt; ctr, Rotation_Matrix&lt; 3 &gt; ori, Coordinate_System *pred, double r, double h, drawNS::APIopenGL3D *_api)']]]
];
