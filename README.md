
# Drone 3D

The program is designed for Linux OS. It simulates a mission on a foreign planet where we can steer drones
that are able to move vertically and horizontally. Planet surface consists of plateaux
(in form of a prism), rectangular plateaux and hills (in a shape of a pyramid).
Furthermore the user is able to add new obstructions and controllable drones. Drones
can land on properly big plateaux and flat surfaces, but they cannot land on hills
and other drones. The simulation is is visualised by the OpenGL API.


## Features

- Three types of obstructions
- Simulated drone's and rotors' move
- 3D visualisation
- Adding new drones and obstructions


## Installation

In order to run the program you need to have the OpenGL installed on your Linux distribution. 
Commands to install it on Ubuntu system:

```bash
  sudo apt-get update
  sudo apt-get install libglu1-mesa-dev freeglut3-dev mesa-common-dev
```
Then, you're ready to run the Drone 3D program from your terminal:
```bash
  git clone https://gitlab.com/bkempa/drone3d.git
  cd drone3d/prj/
  chmod +x Makefile
  make
```

    
## Screenshots

### Menu
[![drone1.png](https://i.postimg.cc/k4sc6vzQ/drone1.png)](https://postimg.cc/Fdfcw39z)

### Simulation
[![drone2.png](https://i.postimg.cc/66HcBzdw/drone2.png)](https://postimg.cc/0rwDfYWH)

Different angle:
[![drone3.png](https://i.postimg.cc/435tBNPZ/drone3.png)](https://postimg.cc/t1sYgb8m)

Sequence animation:
[![Peek-2022-09-14-19-57.gif](https://i.postimg.cc/PrWmP4DP/Peek-2022-09-14-19-57.gif)](https://postimg.cc/mtrcn78s)


## UML

[![Drone3-D-UML.png](https://i.postimg.cc/rpRYNcVf/Drone3-D-UML.png)](https://postimg.cc/qt0LpPCK)
